package main

import (
	"fmt"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/miladsssh/storage_problem/pkg/promotion"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	go promotion.RunConsumer()

	r := gin.Default()

	// Config CORS
	config := cors.DefaultConfig()
	config.AllowAllOrigins = true         // Allow all origins
	config.AllowMethods = []string{"GET"} // Allow only GET, POST requests
	config.AllowHeaders = []string{"Origin", "Content-Length", "Content-Type"}

	r.Use(cors.New(config))
	r.GET("/ping", Heartbeat())
	handler := promotion.NewHandler()

	r.GET("/promotions/:id", handler.GetPromotion())

	r.Run(":1321")

	sig := WaitSignal()
	fmt.Println("received signal " + sig.String() + ", exiting...")

}

func WaitSignal() os.Signal {
	sig := make(chan os.Signal, 1)
	defer close(sig)

	signal.Notify(sig,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)

	return <-sig
}

func Heartbeat() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.String(http.StatusOK, "Pong")
	}
}

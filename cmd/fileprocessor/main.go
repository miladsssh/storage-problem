package main

import (
	"fmt"
	"gitlab.com/miladsssh/storage_problem/pkg/fileprocessor"
	"os"
	"os/signal"
	"syscall"
	"time"
)

const FilePath = "./data/promotions.csv"

func main() {
	p := fileprocessor.NewProcessor()
	ticker := time.NewTicker(5 * time.Minute)

	// Process the file immediately
	p.ProcessFile(FilePath)

	// create a channel to receive OS signals
	sigs := make(chan os.Signal, 1)

	// register the channel to receive signals
	signal.Notify(sigs, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	// this goroutine executes in the background and waits for any signal from the 'sigs' channel
	go func() {
		sig := <-sigs // this will block until a signal is received
		fmt.Println("received signal " + sig.String() + ", exiting...")
		ticker.Stop()
		os.Exit(0) // exit the program when a signal is received
	}()

	// the main goroutine continues to execute the ticker
	for range ticker.C {
		p.ProcessFile(FilePath)
	}
}

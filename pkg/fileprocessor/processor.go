package fileprocessor

import (
	"bufio"
	"encoding/csv"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/miladsssh/storage_problem/infrastructure/eventbus"
	"gitlab.com/miladsssh/storage_problem/infrastructure/eventbus/nsq"
	"os"
	"path/filepath"
	"sync"
	"time"
)

const (
	ChunkSize  = 5000
	ChunkTopic = "chunked_files"
)

type Message struct {
	Command  string `json:"command"`
	FileName string `json:"file_name"`
}

type MsgPublisher interface {
	Publish(msg string) error
}

type Processor struct {
	producer eventbus.Producer
}

func (p *Processor) ProcessFile(path string) {
	log.WithField("path", path).Info("Processing file")

	// send truncate msg to clean the table
	msg := Message{Command: "truncate"}

	err := p.producer.Publish(ChunkTopic, msg)
	if err != nil {
		log.WithField("error", err).Fatal("Failed to send truncate message")
	}

	time.Sleep(2 * time.Second)

	// process the file
	file, err := os.Open(path)
	if err != nil {
		log.WithFields(log.Fields{"error": err, "path": path}).Fatal("Failed to open file")
	}
	defer file.Close()

	reader := csv.NewReader(bufio.NewReader(file))

	var wg sync.WaitGroup
	chunkData := make([][]string, 0, ChunkSize)
	chunkIndex := 0

	for {
		record, err := reader.Read()
		if err != nil {
			break
		}

		chunkData = append(chunkData, record)
		if len(chunkData) == ChunkSize {
			wg.Add(1)
			go func(data [][]string, idx int) {
				defer wg.Done()
				err := p.writeAndPublish(data, idx)
				if err != nil {
					log.WithFields(log.Fields{"error": err, "chunk": idx}).Error("Failed to write and publish chunk")
				}
			}(chunkData, chunkIndex)

			chunkData = make([][]string, 0, ChunkSize)
			chunkIndex++
		}
	}

	// Write and publish the last chunk which might be smaller than ChunkSize
	if len(chunkData) > 0 {
		wg.Add(1)
		go func(data [][]string, idx int) {
			defer wg.Done()
			err := p.writeAndPublish(data, idx)
			if err != nil {
				log.WithFields(log.Fields{"error": err, "chunk": idx}).Error("Failed to write and publish chunk")
			}
		}(chunkData, chunkIndex)
	}

	wg.Wait()
}

func (p *Processor) writeAndPublish(data [][]string, idx int) error {

	title := fmt.Sprintf("chunk-%d.csv", idx)
	filename := fmt.Sprintf("./data/chunks/%s", title)
	err := os.MkdirAll(filepath.Dir(filename), os.ModePerm)
	if err != nil {
		return fmt.Errorf("failed to create directory: %w", err)
	}

	file, err := os.Create(filename)
	if err != nil {
		return fmt.Errorf("failed to create file: %w", err)
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	for _, record := range data {
		err = writer.Write(record)
		if err != nil {
			return fmt.Errorf("failed to write record: %w", err)
		}
	}

	msg := Message{FileName: title}

	err = p.producer.Publish(ChunkTopic, msg)
	if err != nil {
		return fmt.Errorf("failed to publish message: %w", err)
	}

	log.WithField("filename", filename).Info("Chunk written and message published")
	return nil
}

func NewProcessor() *Processor {
	log.Info("Creating new Processor")
	return &Processor{
		producer: nsq.NewMessageBus(),
	}
}

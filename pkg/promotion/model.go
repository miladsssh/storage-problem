package promotion

import (
	"github.com/google/uuid"
	"strconv"
	"time"
)

type Promotion struct {
	ID             int64     `json:"-"`
	UUID           uuid.UUID `json:"id"`
	Price          float64   `json:"price"`
	FileName       string    `json:"-"`
	CreatedAt      time.Time `json:"-"`
	ExpirationDate time.Time `json:"expiration_date"`
}

func NewPromotionModel(id int64) *Promotion {
	return &Promotion{
		ID: id,
	}
}

func NewPromotion(p []string, fileName string) *Promotion {
	if len(p) == 0 {
		return nil
	}

	uuid, _ := uuid.Parse(p[0])
	floatValue, _ := strconv.ParseFloat(p[1], 64)
	return &Promotion{
		UUID:           uuid,
		Price:          floatValue,
		FileName:       fileName,
		ExpirationDate: convertDate(p[2]),
		CreatedAt:      time.Now(),
	}
}

func convertDate(d string) time.Time {
	layout := "2006-01-02 15:04:05 -0700 MST"
	date, _ := time.Parse(layout, d)

	return date
}

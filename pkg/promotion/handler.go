package promotion

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type Handler struct {
	processor Processor
}

func NewHandler() *Handler {
	return &Handler{processor: NewProcessor()}
}

func (h *Handler) GetPromotion() gin.HandlerFunc {
	return func(c *gin.Context) {
		idStr := c.Param("id")

		// Parse the id to integer and check if it's valid
		id, err := strconv.Atoi(idStr)
		if err != nil || id <= 0 {
			c.JSON(http.StatusBadRequest, gin.H{
				"error": "Invalid ID",
			})
			return
		}

		// Use the processor to get the promotion
		promotion, err := h.processor.Get(int64(id))
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"error": "Error retrieving the promotion",
			})
			return
		}

		if promotion == nil {
			c.JSON(http.StatusNotFound, gin.H{
				"error": "Promotion not found",
			})
			return
		}

		// Respond with the promotion
		c.JSON(http.StatusOK, promotion)
	}
}

package promotion

import (
	"context"
	"github.com/jackc/pgx/v5"
	log "github.com/sirupsen/logrus"
	"gitlab.com/miladsssh/storage_problem/infrastructure/database"
	"gitlab.com/miladsssh/storage_problem/infrastructure/database/posgresql"
)

var (
	promotionTableName = "storage_problem.promotions"
	promotionTableCols = []string{
		"promotion_uuid",
		"promotion_price",
		"promotion_import_file_name",
		"promotion_expiration_date",
		"promotion_created_at",
	}
)

type PromotionStore interface {
	BulkInsertPromotions(context.Context, []*Promotion) error
	TruncateTable(context.Context) error
	Get(context.Context, *Promotion) (*Promotion, error)
}

type store struct {
	sql database.SQL
}

func (s *store) TruncateTable(ctx context.Context) error {
	log.Info("Truncating table")
	if err := s.sql.Truncate(ctx); err != nil {
		log.Error("Failed to truncate table: ", err)
		return err
	}

	return nil
}

func (s *store) BulkInsertPromotions(ctx context.Context, promotions []*Promotion) error {
	log.WithField("count", len(promotions)).Info("Inserting promotions in bulk")

	tx, err := s.sql.Tx()
	if err != nil {
		log.Error("Failed to start transaction: ", err)
		return err
	}

	defer tx.RollbackUnlessCommitted()

	data := make([][]interface{}, len(promotions))
	for i, promotion := range promotions {
		data[i] = []interface{}{
			promotion.UUID,
			promotion.Price,
			promotion.FileName,
			promotion.ExpirationDate,
			promotion.CreatedAt,
		}
	}

	if err := s.sql.InsertBulkTx(ctx, tx, data); err != nil {
		log.Error("Failed to bulk insert promotions: ", err)
		return err
	}

	if err := tx.Commit(); nil != err {
		log.Error("Failed to commit transaction: ", err)
		return err
	}

	return nil
}

func (s *store) Get(ctx context.Context, p *Promotion) (*Promotion, error) {
	log.WithField("GET", p.ID).Info("Get promotion")

	var promotion Promotion
	handleRow := func(rows pgx.Rows) error {
		var p Promotion
		if err := rows.Scan(&p.ID, &p.UUID, &p.Price, &p.FileName, &p.ExpirationDate, &p.CreatedAt); err != nil {
			return err
		}
		promotion = p
		return nil
	}

	if err := s.sql.Select(ctx, database.Query{
		Condition:     "promotion_id",
		ConditionArgs: p.ID,
	}, handleRow); err != nil {
		log.Error("Failed to Select promotions: ", err)
		return nil, err
	}

	return &promotion, nil
}

func NewStore() PromotionStore {
	log.Info("Creating new store")
	return &store{
		sql: posgresql.NewPostgreSQL(
			promotionTableName, promotionTableCols),
	}
}

package promotion

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"gitlab.com/miladsssh/storage_problem/infrastructure/eventbus"
	"gitlab.com/miladsssh/storage_problem/infrastructure/eventbus/nsq"
)

type Message struct {
	Command  string `json:"command"`
	FileName string `json:"file_name"`
}

func RunConsumer() {
	builder := nsq.NewConsumerBuilder()
	builder = builder.WithHandler(NewChunkProcessor())

	if err := builder.Run(); err != nil {
		log.Fatal(err)
	}
}

type chunkProcessor struct {
	processor Processor
}

func (c chunkProcessor) Topic() string {
	return "chunked_files"
}

func (c chunkProcessor) Channel() string {
	return "promotion.consumer"
}

func (c chunkProcessor) HandleMessage(message []byte) error {
	log.Infof("Received message on topic '%s' in channel '%s'", c.Topic(), c.Channel())

	var myMessage Message
	_ = json.Unmarshal(message, &myMessage)

	if myMessage.Command == "truncate" {
		if err := c.processor.Truncate(); err != nil {
			log.Error(err)
			return err
		}

		return nil
	}

	if err := c.processor.BulkAddPromotions(myMessage.FileName); err != nil {
		log.Error(err)
		return err
	}

	return nil
}

func (c chunkProcessor) HandlerConcurrency() int {
	return 5
}

func NewChunkProcessor() eventbus.ConsumerHandler {
	return &chunkProcessor{
		processor: NewProcessor(),
	}
}

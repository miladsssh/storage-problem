package promotion

import (
	"context"
	"encoding/csv"
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"
)

const ChunkSize = 1000

type Processor interface {
	BulkAddPromotions(string) error
	Truncate() error
	Get(int64) (*Promotion, error)
}

type processor struct {
	store PromotionStore
}

func (p *processor) Get(id int64) (*Promotion, error) {
	pm := NewPromotionModel(id)
	promotion, err := p.store.Get(context.Background(), pm)
	if err != nil {
		log.WithField("Error fetching the promotion", id).Fatal(err)
	}

	if promotion.ID == 0 {
		return nil, nil
	}

	return promotion, nil
}

func (p *processor) BulkAddPromotions(fileName string) error {
	absolutePath, err := filepath.Abs("./data/chunks/" + fileName)
	if err != nil {
		log.WithField("fileName", fileName).Fatal(err)
	}

	file, err := os.Open(absolutePath)
	if err != nil {
		log.WithField("filePath", absolutePath).Error(err)
		return err
	}
	defer file.Close()

	reader := csv.NewReader(file)

	records, err := reader.ReadAll()
	if err != nil {
		log.WithField("filePath", absolutePath).Error("Failed to read all records: ", err)
		return err
	}

	log.WithFields(log.Fields{
		"filePath": absolutePath,
		"count":    len(records),
	}).Info("Processing records")

	for i := 0; i < len(records); i += ChunkSize {
		end := i + ChunkSize
		if end > len(records) {
			end = len(records)
		}

		chunk := records[i:end]

		promotions := make([]*Promotion, len(chunk))
		for i, record := range chunk {
			promotion := NewPromotion(record, fileName)
			promotions[i] = promotion
		}

		if err := p.store.BulkInsertPromotions(context.TODO(), promotions); err != nil {
			log.WithFields(log.Fields{
				"filePath": absolutePath,
				"start":    i,
				"end":      end,
			}).Error("Failed to bulk insert promotions: ", err)
			return err
		}
	}

	return nil

}

func (p *processor) Truncate() error {
	if err := p.store.TruncateTable(context.TODO()); err != nil {
		log.Error("Failed to truncate table: ", err)
		return err
	}

	return nil
}

func NewProcessor() Processor {
	return &processor{
		store: NewStore(),
	}
}

FROM alpine:latest

RUN mkdir /app

WORKDIR /app

COPY build/promotionApp /app

CMD [ "/app/promotionApp" ]
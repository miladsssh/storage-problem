# Storage problem



## Architecture
![architecture](https://i.imgur.com/kIReldF.png)

I see this application in 2 separated microservices. 
 * FileProcessor: this application checks every 30 minutes (in the code every 5 minutes) in the storage for a new file
   1. The FileProcessor periodically checks storage for new files every 30 minutes, breaking down the data into manageable chunks (each consisting of 5000 lines).
   2. Initially, it publishes a message to truncate the existing table
   3. Subsequently, it publishes the names of each chunk file.
 
    **Files should be upload to s3 and s3 should publish a msg and FileProcessor service should listen to this event**
 * Promotion Service:
   1. The Promotion Service starts by listening to the published events from FileProcessor.
   2. Upon receiving these events, it attempts a bulk insert of the data chunks.
   3. A logging mechanism records each process, allowing us to review the progress and identify any issues.
   4. Every chunk operation is encapsulated in a transaction to ensure data integrity. If a failure occurs, we can pinpoint the specific portion of data that has not been inserted.
   5. Finally, the Promotion Service exposes an API to retrieve the data.
```
make up
DATABASE_URL="postgresql://postgres:postgres@host.docker.internal:5432/verve_group?sslmode=disable" make migration-up
make up_build_services

curl http://localhost:1321/promotions/123 -> should return a json files
curl http://localhost:1321/promotions/0 -> should return Not found
```

***

### The .csv file could be very big (billions of entries) - how would your application perform?
1. We handle large datasets by chunking the data and using multiple services to process the chunks concurrently. This approach increases performance and reduces the chance of out-of-memory errors.
2. Although the current architecture employs just one Promotion service, it's scalable and can be expanded to handle larger datasets.
3. We use PostgreSQL for data persistence, but if data access speed is a priority, Redis could be an alternative as it stores data in memory. However, the cost and memory requirements of Redis need to be evaluated carefully.
4. Our architecture employs an abstract store interface, allowing us to switch between data stores (like PostgreSQL and Redis) with minimal changes. This level of flexibility is beneficial for handling changing requirements or improving performance.
5. Increasing the number of consumers can also assist in processing large amounts of data more quickly.

### How would your application perform in peak periods (millions of requests per minute)?
1. To handle high request volumes, we can separate databases for read and write operations. In addition, we can deploy multiple read replicas to maintain high availability and performance.
2. The Promotion service is scalable and can be independently scaled out to meet high demand.
3. A properly configured load balancer can help distribute the load and prevent any single service from becoming a bottleneck.

### How would you operate this app in production (e.g. deployment, scaling, monitoring)?
1. Our application's modular design simplifies deployment. Each service can be deployed independently, allowing for efficient updates and rollbacks.
2. Centralized logging is crucial for monitoring the system's health and debugging issues. Tools like DataDog or the ELK stack (Elasticsearch, Logstash, Kibana) stack can be used for log aggregation and analysis.
3. Scaling is straightforward due to the separate, modular nature of our services. We can scale each service independently, depending on its load and performance.
4. For monitoring, we can integrate a solution like Prometheus and Grafana. 
5. We should also consider implementing health checks and readiness probes for our services to ensure that any issues can be detected and addressed as quickly as possible.
6. Additionally, considering an automated CI/CD pipeline for code integration, testing, and deployment would be beneficial. 
7. It is better to use a correlation id in each context to make our application easier to trace in case of a failure.

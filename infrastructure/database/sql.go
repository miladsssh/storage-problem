package database

import (
	"context"
	"github.com/jackc/pgx/v5"
	"io"
)

type (
	Query struct {
		Value         interface{}
		Condition     interface{}
		ConditionArgs interface{}
	}

	SQL interface {
		io.Closer
		InsertBulkTx(context.Context, Transaction, [][]interface{}) error
		Truncate(context.Context) error
		Select(context.Context, Query, func(rows pgx.Rows) error) error
		TransactionSource
	}
	TransactionSource interface {
		Tx() (Transaction, error)
	}

	Transaction interface {
		Exec(string, ...interface{}) error
		RollbackUnlessCommitted()
		Commit() error
	}
)

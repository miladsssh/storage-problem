package posgresql

import (
	"fmt"
	"github.com/caarlos0/env"
)

var (
	conf *Conf
)

func init() {
	conf = new(Conf)
	_ = env.Parse(conf)
}

type (
	Conf struct {
		Host        string `env:"DB_PG_HOST"`
		Username    string `env:"DB_PG_USER" envDefault:"postgres"`
		Password    string `env:"DB_PG_PASS" envDefault:"rootPassword"`
		DBName      string `env:"DB_PG_NAME" envDefault:"postgres"`
		SSLMode     string `env:"DB_PG_SSL"  envDefault:"disable"`
		MaxOpenConn int    `env:"DB_PG_MAX_OPEN_CONN"  envDefault:"5"`
		MaxIdleConn int    `env:"DB_PG_MAX_IDLE_CONN"  envDefault:"5"`
		MaxConnLife int    `env:"DB_PG_MAX_CONN_LIFE"  envDefault:"5000"`
	}
)

func (c *Conf) String() string {
	return fmt.Sprintf(
		"postgresql://%s:%s@%s/%s?sslmode=%s",
		c.Username,
		c.Password,
		c.Host,
		c.DBName,
		c.SSLMode,
	)
}

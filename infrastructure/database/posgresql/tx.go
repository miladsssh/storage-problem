package posgresql

import (
	"context"
	"github.com/jackc/pgx/v5"
	"github.com/opentracing/opentracing-go/log"
	"gitlab.com/miladsssh/storage_problem/infrastructure/database"
)

type (
	transaction struct {
		tx pgx.Tx
	}
)

func (t *transaction) Exec(sql string, args ...interface{}) error {
	if _, err := t.tx.Exec(context.Background(), sql, args...); err != nil {
		return err
	}

	return nil
}

func (t *transaction) RollbackUnlessCommitted() {
	if err := t.tx.Rollback(context.Background()); err != nil {
		log.Error(err)
	}
}

func (t *transaction) Commit() error {
	if err := t.tx.Commit(context.Background()); err != nil {
		log.Error(err)
		return err
	}

	return nil
}

func (p *postgresql) Tx() (database.Transaction, error) {
	tx, err := p.conn.Begin(context.Background())
	if err != nil {
		log.Error(err)
		return nil, err
	}

	return &transaction{tx: tx}, nil
}

func (p *postgresql) Close() error {
	p.conn.Close()
	return nil
}

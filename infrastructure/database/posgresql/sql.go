package posgresql

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/sirupsen/logrus"
	"gitlab.com/miladsssh/storage_problem/infrastructure/database"
	"strings"
)

type (
	postgresql struct {
		tableName string
		columns   []string
		conn      *pgxpool.Pool
	}
)

func (p *postgresql) InsertBulkTx(ctx context.Context, transaction database.Transaction, data [][]interface{}) error {
	// Get the underlying pgx transaction
	tx := transaction

	placeholders := make([]string, 0, len(data))
	flattenedData := make([]interface{}, 0, len(data)*len(p.columns))
	for i, rowData := range data {
		rowPlaceholders := make([]string, len(p.columns))
		for j := range p.columns {
			rowPlaceholders[j] = fmt.Sprintf("$%d", i*len(p.columns)+j+1)
		}
		placeholders = append(placeholders, "("+strings.Join(rowPlaceholders, ", ")+")")

		flattenedData = append(flattenedData, rowData...)
	}

	stmt := fmt.Sprintf(
		"INSERT INTO %s (%s) VALUES %s",
		p.tableName,
		strings.Join(p.columns, ", "),
		strings.Join(placeholders, ", "),
	)

	err := tx.Exec(stmt, flattenedData...)
	if err != nil {
		logrus.Errorf("failed to execute bulk insert: %v", err)
		return err
	}

	logrus.Infof("Bulk insert successful: %d rows inserted", len(data))

	return nil
}

func (p *postgresql) Select(ctx context.Context, query database.Query, handleRow func(pgx.Rows) error) error {
	stmt := fmt.Sprintf("SELECT * FROM %s WHERE %s = $1", p.tableName, query.Condition)
	rows, err := p.conn.Query(ctx, stmt, query.ConditionArgs)
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		err := handleRow(rows)
		if err != nil {
			logrus.Errorf("error handling row: %v", err)
			return err
		}
	}

	if err = rows.Err(); err != nil {
		return err
	}

	return nil
}

func (p *postgresql) Truncate(ctx context.Context) error {
	_, err := p.conn.Exec(ctx, fmt.Sprintf("TRUNCATE TABLE %s RESTART IDENTITY", p.tableName))
	if err != nil {
		return fmt.Errorf("failed to truncate the table %s, error: %w", p.tableName, err)
	}

	logrus.Infof("Table %s truncated successfully", p.tableName)

	return nil
}

package posgresql

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/miladsssh/storage_problem/infrastructure/database"
	"os"
)

var (
	pgSession *pgxpool.Pool
)

func newSession(conn string, maxOpen, maxIdle, maxLifeTime int) *pgxpool.Pool {

	config, err := pgxpool.ParseConfig(conn)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to parse database connection string: %v\n", err)
		os.Exit(1)
	}

	// Set the maximum connections in the pool
	config.MaxConns = 50

	// Create the pool
	connection, err := pgxpool.NewWithConfig(context.Background(), config)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to connect to database: %v\n", err)
		os.Exit(1)
	}

	if err = connection.Ping(context.Background()); err != nil {
		fmt.Fprintf(os.Stderr, "Unable to connect to database: %v\n", err)
		os.Exit(1)
	}

	return connection
}

func NewPostgreSQL(tn string, c []string) database.SQL {
	if nil == pgSession {
		pgSession = newSession(
			conf.String(),
			conf.MaxOpenConn,
			conf.MaxIdleConn,
			conf.MaxConnLife,
		)
	}
	return &postgresql{
		tableName: tn,
		columns:   c,
		conn:      pgSession,
	}
}

package eventbus

import (
	"io"
)

type (
	Producer interface {
		io.Closer
		Publish(ch string, msg interface{}) error
	}
)

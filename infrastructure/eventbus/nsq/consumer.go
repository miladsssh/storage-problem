package nsq

import (
	"github.com/caarlos0/env"
	"github.com/nsqio/go-nsq"
	"gitlab.com/miladsssh/storage_problem/infrastructure/eventbus"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"
)

type (
	Config struct {
		Host           string `env:"EVENT_CONSUMER_HOST" envSeparator:","`
		MessageTimeout time.Duration
	}
)

type consumerBuilder struct {
	host           string
	messageTimeout time.Duration
	consumer       *nsq.Consumer
}

func (c consumerBuilder) Run() error {
	err := c.consumer.ConnectToNSQLookupd(c.host)
	if err != nil {
		log.Fatal("Error connecting to NSQ:", err)
	}

	// Graceful shutdown
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)
	<-sigChan
	c.consumer.Stop()
	return nil
}

func (c consumerBuilder) WithHandler(h eventbus.ConsumerHandler) eventbus.ConsumerBuilder {
	conf := nsq.NewConfig()
	if c.messageTimeout != 0 {
		conf.MsgTimeout = c.messageTimeout
	}

	consumer, err := nsq.NewConsumer(
		h.Topic(),
		h.Channel(),
		conf,
	)
	if nil != err {
		panic(err)
	}

	wrapper := WrapHandlerFunc(h)
	consumer.AddHandler(wrapper)
	consumer.AddConcurrentHandlers(wrapper, h.HandlerConcurrency())
	c.consumer = consumer

	return c
}

// WrapHandlerFunc wraps the ConsumerHandler's HandleMessage function to adapt it to nsq.HandlerFunc
func WrapHandlerFunc(handler eventbus.ConsumerHandler) nsq.HandlerFunc {
	return func(message *nsq.Message) error {
		// Here we call the original HandleMessage function with the appropriate type conversion
		return handler.HandleMessage(message.Body)
	}
}

func NewConsumerBuilder() eventbus.ConsumerBuilder {
	c := new(Config)
	_ = env.Parse(c)

	return &consumerBuilder{
		host:           c.Host,
		messageTimeout: c.MessageTimeout,
	}
}

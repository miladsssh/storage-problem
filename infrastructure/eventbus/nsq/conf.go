package nsq

type (
	ProducerConf struct {
		Host string `env:"EVENT_PRODUCER_HOST"`
	}
	ConsumerConf struct {
		Host        []string `env:"EVENT_CONSUMER_HOST" envSeparator:","`
		Topic       string   `env:"EVENT_CONSUMER_TOPIC"`
		Channel     string   `env:"EVENT_CONSUMER_CHANNEL"`
		MaxInFlight int      `env:"EVENT_CONSUMER_MAX_INFLIGHT" envDefault:"200"`
		Concurrency int      `env:"EVENT_CONSUMER_CONCURRENCY" envDefault:"25"`
	}
)

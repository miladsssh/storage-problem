package nsq

import (
	"encoding/json"
	"fmt"
	"github.com/caarlos0/env"
	"github.com/nsqio/go-nsq"
	"gitlab.com/miladsssh/storage_problem/infrastructure/eventbus"
	"sync"
)

type (
	nsqProducer interface {
		Publish(string, []byte) error
		Stop()
	}

	nsqBus struct {
		nsqProducer nsqProducer
		marshaller  func(interface{}) []byte
		wg          *sync.WaitGroup
	}
)

func (bus *nsqBus) Publish(topic string, msg interface{}) error {
	bus.wg.Add(1)
	defer bus.wg.Done()
	if err := bus.nsqProducer.Publish(topic, bus.marshaller(msg)); nil != err {
		fmt.Printf("some error here: %s", err)

		return err
	}

	return nil
}

func (bus *nsqBus) Close() error {
	bus.wg.Wait()
	bus.nsqProducer.Stop()

	return nil
}

func NewMessageBus() eventbus.Producer {

	c := new(ProducerConf)
	_ = env.Parse(c)

	producer, err := nsq.NewProducer(c.Host, nsq.NewConfig())
	if nil != err {
		panic(err)
	}

	if err := producer.Ping(); nil != err {
		panic(err)
	}

	return &nsqBus{
		marshaller:  MessageMarshaller,
		nsqProducer: producer,
		wg:          new(sync.WaitGroup),
	}
}

type Message struct {
	FileName string `json:"file_name"`
}

func MessageMarshaller(message interface{}) []byte {
	r, err := json.Marshal(message)
	if nil != err {
		return nil
	}

	return r
}

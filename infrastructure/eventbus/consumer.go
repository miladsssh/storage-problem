package eventbus

type (
	// ConsumerHandler interface represents a single message consumer
	ConsumerHandler interface {
		Topic() string
		Channel() string
		HandleMessage(message []byte) error
		HandlerConcurrency() int
	}

	ConsumerBuilder interface {
		WithHandler(ch ConsumerHandler) ConsumerBuilder
		Run() error
	}
)

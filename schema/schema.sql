SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: storage_problem; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA storage_problem;


SET default_tablespace = '';

--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version character varying(255) NOT NULL
);


--
-- Name: promotions; Type: TABLE; Schema: storage_problem; Owner: -
--

CREATE TABLE storage_problem.promotions (
    promotion_id bigint NOT NULL,
    promotion_uuid uuid,
    promotion_price real,
    promotion_import_file_name character varying(255),
    promotion_expiration_date timestamp without time zone,
    promotion_created_at timestamp without time zone
);


--
-- Name: promotions_promotion_id_seq; Type: SEQUENCE; Schema: storage_problem; Owner: -
--

CREATE SEQUENCE storage_problem.promotions_promotion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: promotions_promotion_id_seq; Type: SEQUENCE OWNED BY; Schema: storage_problem; Owner: -
--

ALTER SEQUENCE storage_problem.promotions_promotion_id_seq OWNED BY storage_problem.promotions.promotion_id;


--
-- Name: promotions promotion_id; Type: DEFAULT; Schema: storage_problem; Owner: -
--

ALTER TABLE ONLY storage_problem.promotions ALTER COLUMN promotion_id SET DEFAULT nextval('storage_problem.promotions_promotion_id_seq'::regclass);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: promotions promotions_pkey; Type: CONSTRAINT; Schema: storage_problem; Owner: -
--

ALTER TABLE ONLY storage_problem.promotions
    ADD CONSTRAINT promotions_pkey PRIMARY KEY (promotion_id);


--
-- PostgreSQL database dump complete
--


--
-- Dbmate schema migrations
--

INSERT INTO public.schema_migrations (version) VALUES
    ('20230723151047'),
    ('20230723151102');

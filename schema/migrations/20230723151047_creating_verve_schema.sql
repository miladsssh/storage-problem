-- migrate:up
CREATE SCHEMA IF NOT EXISTS storage_problem;

-- migrate:down
DROP SCHEMA IF EXISTS storage_problem CASCADE;

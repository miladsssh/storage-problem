-- migrate:up
CREATE TABLE IF NOT EXISTS storage_problem.promotions (
    promotion_id BIGSERIAL PRIMARY KEY,
    promotion_uuid UUID,
    promotion_price REAL,
    promotion_import_file_name VARCHAR(255),
    promotion_expiration_date TIMESTAMP,
    promotion_created_at TIMESTAMP
);


-- migrate:down
DROP TABLE IF EXISTS promotions;

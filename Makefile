PROMOTION_BINARY = promotionApp
FILE_PROCESSOR_BINARY = fileprocessorApp


up:
	@echo "Stopping docker images (if running...)"
	docker-compose down
	@echo "Building and starting docker images..."
	docker-compose up --build -d db migration nsqlookupd nsqd nsqadmin
	@echo "Docker images built and started!"

down:
	@echo "Stopping docker compose..."
	docker-compose down
	@echo "Done!"

new-migration:
	docker run --rm -v $(PWD)/schema:/db amacneil/dbmate new ${NAME}

migration-up:
	docker run -e DATABASE_URL --rm -v $(PWD)/schema:/db amacneil/dbmate up

migration-down:
	docker run -e DATABASE_URL --rm -v $(PWD)/schema:/db amacneil/dbmate down

build_promotion:
	@echo "Building promotion service binary..."
	cd ./cmd/promotion && env GOOS=linux CGO_ENABLED=0 go build -o ../../build/${PROMOTION_BINARY}
	@echo "Done!"

build_fileprocessor:
	@echo "Building fileprocessor service binary..."
	cd ./cmd/fileprocessor && env GOOS=linux CGO_ENABLED=0 go build -o ../../build/${FILE_PROCESSOR_BINARY}
	@echo "Done!"

up_build_services: build_promotion build_fileprocessor
	@echo "Stopping docker images (if running...)"
	docker-compose down promotion-service fileprocessor-service
	@echo "Building and starting docker images..."
	docker-compose up --build -d promotion-service fileprocessor-service
	@echo "Docker images built and started!"
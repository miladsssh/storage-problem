FROM alpine:latest

RUN mkdir /app
RUN mkdir /app/data

WORKDIR /app

COPY build/fileprocessorApp /app
COPY data/promotions.csv /app/data

CMD [ "/app/fileprocessorApp" ]